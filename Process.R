library("reshape")
library("ggplot2")
library("plyr")
library("xts")
options(stringsAsFactors=FALSE)
processVolSurfaceCsv <- function (dataDirLoad = getwd(), dataDirWrite = getwd(), returnDataFrame = TRUE, writeDataFrame = TRUE)
{  
  filelist <- list.files(dataDirLoad, full.names=TRUE,pattern="*.csv")
  initial <- read.csv(filelist[[1]], nrows = 3)
  colTypes <- sapply(initial, class)
  list_of_files <- lapply(filelist, read.csv, colClasses = colTypes)
  mergedDataSet <- merge_all(dfs=list_of_files)
  mergedDataSet$date <- as.Date(mergedDataSet$date,format="%m/%d/%y")
  splitMergedDataSet <- split(mergedDataSet,mergedDataSet$symbol)
  if (writeDataFrame == TRUE)
  {saveRDS(splitMergedDataSet, paste0(dataDirWrite,"/VolSurface.rds"),compress=TRUE)}
  if (returnDataFrame == TRUE)
  {processVolSurfaceCsv <- splitMergedDataSet}

}

processVolSurfaceCsv("./RawData",returnDataFrame=FALSE)







